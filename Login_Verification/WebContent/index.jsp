<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<form action="" method="post">
	<p>
      	<label>输入图中的验证码</label>
      	<input type="text" name="valitext"  /><span id="valitextTip"></span><br/>
      	<img id="valiimg" src="<%=request.getContextPath() %>/createCodeServlet" alt="验证码加载失败" />
      	<a id="changeViliText" href="javascript:void(0);">看不清，换一张</a>
     </p>
      	<!-- 电话验证 -->
   	<p>
   		<label>请输入您的手机号：</label><br/>
    	<input type="text" name="phoneNum"/>
    	<input id="getvilaPhoneText" type="button" value="点击获取验证码"/>
    	<br/>
    	<input type="text" name="valiPhoneText" placeholder="输入验证码"/>
    	<span id="tipPhoneText"></span><br/>
   	</p>
   	 <input type="submit" value="提交" class="opt_sub" />
</form>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.12.4.js"></script>
	<script type="text/javascript">
	$(function(){
		/*看不清，换一张*/
		$("#changeViliText").click(function(){
			$("#valiimg").attr("src","<%=request.getContextPath() %>/createCodeServlet?d="+new Date());
			$("input[name='valitext']").val("");
		});
			/* 光标离开时进行图形验证码的验证 */
			$("input[name='valitext']").blur(checkCode);
			function checkCode(){
				//获取用户填写的验证码
				var valitext=$("input[name='valitext']").val();
				$.ajax({
					url:"<%= request.getContextPath()%>/dealCodeServlet",
					type:"post",
					data:"valitext="+valitext+"&time="+new Date(),//new Date()为了让会话认识，时每一次不同请求
					dataType:"text",
					success: function(data){
						var valiTip=$("#valitextTip");
						 if(data=="true"){
							valiTip.html("验证码输入正确！");	 	
							valiTip.css("color","green");
						 }else{
							valiTip.html("验证码输入错误！");	 	
							valiTip.css("color","red");							 
						 }
					}
					
				});
			};
			//定时函数id号
			var clock;
			//获取验证码倒计时时间
			var nums=20;
			//按钮对象
			var btn;
			/* 点击获取手机验证码 */
		$("#getvilaPhoneText").click(function(){
			$("#tipPhoneText").html("");//清空提示
			//获取手机号
			var phoneNum = $("input[name='phoneNum']").val();
			if(phoneNum==null || phoneNum=="" || phoneNum.trim().length==0){
				$("#tipPhoneText").html("电话号码不能为空！").css("color","red");
			}else if(isNaN(phoneNum)){
				$("#tipPhoneText").html("请填写正确的电话！").css("color","red");
			
			}else{
				$.post("<%= request.getContextPath()%>/sendSMSServlet","tel="+phoneNum);
				//将获取验证码的按钮置为不可用
				btn=this;
				btn.disabled=true;
				//按钮上显示文字改为（重新获取（倒计时5s））
				btn.value="重新获取("+nums+")";
				//定时函数(每秒执行一次)
				clock=setInterval(loop,1000);
			}
		});
		/* 定时函数：每秒将等待时间-1；到0，获取验证码设置为可用，文字改为“点击发送验证码”；否则重新获取（倒计时） */
			function loop(){
				//时间-1
				nums--;
				if(nums>0){
					btn.value="重新获取("+nums+")";
				}else{
					clearInterval(clock);
					btn.disabled=false;
					btn.value="重新获取验证码";
					nums=20;//时间重置
				}
			}
			/* 光标离开时的电话验证码  */
			$("input[name='valiPhoneText']").blur(function(){
				var tipPhoneText=$("#tipPhoneText");
				//获取验证码
				var inputCode=$("input[name='valiPhoneText']").val();
				if(inputCode=="" || inputCode==null){
					tipPhoneText.html("验证码不能为空！");
				}else{
					$.get("<%= request.getContextPath()%>/valiPhoneCodeServlet","inputCode="+inputCode,function(data){
						if(data=="ture"){
							  tipPhoneText.html("输入正确");
							  tipPhoneText.css("color","green");
						}else{
							  tipPhoneText.html("输入错误！");							
							  tipPhoneText.css("color","red");
						}
					});
				}
			});
	});
	</script>
</body>
</html>