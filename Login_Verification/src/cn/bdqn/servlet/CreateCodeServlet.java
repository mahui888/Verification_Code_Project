package cn.bdqn.servlet;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CreateCodeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//创建随机类
		Random r = new Random();
		//确定生成图片的大小
		int w = 100;
		int h = 40;
		//创建画板(设定宽高，颜色模式)
		BufferedImage board = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		//创建画笔
		Graphics pen = board.getGraphics();
		//调背景颜色----------背景颜色调制淡色
		Color c_bg = new Color(200+r.nextInt(40),200+r.nextInt(40),200+r.nextInt(40));
		//画笔蘸颜色-----准备画背景
		pen.setColor(c_bg);
		//画背景
		pen.fillRect(0, 0, w, h);
		/*生成验证码*/
		StringBuffer code = new StringBuffer();
		//用for循环生成四个验证码
		for(int i =1;i<=4;i++){
			//调制验证码颜色
			Color c_num = new Color(100+r.nextInt(40),100+r.nextInt(40),100+r.nextInt(40));
			//画笔蘸颜色
			pen.setColor(c_num);
			//设置绘制的字体
			Font f_num = new Font("隶书", Font.BOLD, 25);
			//画笔注入字体
			pen.setFont(f_num);
			/*随机生成*/
			int num1 = 65+r.nextInt(26);//大写字母
			int num2 = 97+r.nextInt(26);//小写字母
			int num3 = r.nextInt(10);	//0~9的数字
			/*转换成对应的字符*/
			String s1 = String.valueOf((char)num1);	//大写字母
			String s2 = String.valueOf((char)num2);	//小写字母
			String s3 = String.valueOf(num3);		//0~9的数字
			
			int n = 0;
			int n1 = 0;
			String s4 = "";
			//给画板上显示生成的每一个字符串
			if((n = r.nextInt(2))>0.5){
				if((n1=r.nextInt(2))>0.5){
					s4 = s1;	//大写字母
				}else{
					s4 = s2;	//小写字母
				}
				
			}else{
				s4 = s3;		//数字
			}
			//画干扰线（防机器人自动识别）
			for(int j = 0;j<2;j++){
				int x = r.nextInt(w-2);
				int y = r.nextInt(h-2);
				int x1 = r.nextInt(20);
				int y1 = r.nextInt(30);
				pen.drawLine(x, y, x1, y1);
			}
			pen.drawString(s4, 17*i, 30);//挪动画笔书写位置
			if(n>0.5&&n1>0.5){
				code = code.append(s1);
			}else if(n>0.5&&n1<0.5){
				code = code.append(s2);
			}else{
				code = code.append(s3);
			}
			
		}
		/*将生成的验证码存放到session域中*/
		HttpSession session = req.getSession();
		session.setAttribute("valitext", code.toString());
		//将画好的画布压缩成图片进行输出
		ImageIO.write(board, "jpeg", resp.getOutputStream());
		System.out.println(board+"jpeg"+resp.getOutputStream());
		
			
	}
}







