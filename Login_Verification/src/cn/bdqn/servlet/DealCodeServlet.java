package cn.bdqn.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DealCodeServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out = resp.getWriter();
		String valitext=req.getParameter("valitext");//用户填写的验证码
		System.out.println("用户填写的验证码："+valitext); 
		//从回话域中取出生成的验证码内容
		HttpSession session=req.getSession();
		//服务器生成的验证码
		String code=(String)session.getAttribute("valitext");
		System.out.println("服务器中的验证码："+code);
		//比较
		if(valitext.equalsIgnoreCase(code)){
				out.print("true");
		}else {
			out.print("false");
		}
		out.flush();
		out.close();
	}

}
