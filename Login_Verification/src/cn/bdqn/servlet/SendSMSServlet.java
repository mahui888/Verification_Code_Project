package cn.bdqn.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.bdqn.utils.SendSMSUtil;
/**
 * 发送短信的servlet
 * @author Administrator
 *
 */
public class SendSMSServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		//获取电话号码
		String tel=request.getParameter("tel");
		System.out.println("用户的电话号码："+tel);
		//生成6位数验证码
		String code=SendSMSUtil.getCode();
		System.out.println("生成的6位验证码："+code);
		HttpSession session = request.getSession();
		/*测试成功过后发送到运营商*/
		String result=SendSMSUtil.send("{\"code\":\""+code+"\"}", tel);
		System.out.println(result+"**");
		//将生成的验证码存放到session中
		session.setAttribute("code", code);
		
	}
	

}
