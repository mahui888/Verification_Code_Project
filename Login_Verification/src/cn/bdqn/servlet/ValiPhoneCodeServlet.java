package cn.bdqn.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ValiPhoneCodeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		//获取用户填写的6位验证码
		String inputCode=request.getParameter("inputCode");
		//从session中取出生成的验证码
		HttpSession session = request.getSession();
		String code=(String)session.getAttribute("code");
		System.out.println(inputCode+"*****************"+code);
		if(code.trim().equals(inputCode.trim())){
			out.print("true");
		}else{
			out.print("false");
		}
		out.flush();
		out.close();
	}

}
