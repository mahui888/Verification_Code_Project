package cn.bdqn.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;



/**
 * 发短信，生成6位数字，的工具类
 * @author Administrator
 *
 */
public class SendSMSUtil {
	private static final String addr="http://api.sms.cn/sms/";
	private static final String userId="mahui888";
	private static final String pwd="f4b240baf26d0d8b9862c64ed889590c";
	private static final String encode="utf8";
	
	/**
	 * 发送短信（测试成功以后再写）
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 */
	public static String send(String msgContent,String mobile) throws UnsupportedEncodingException, IOException{
		//组件请求
		/*String strAddr=addr+"?ac=send&uid="+userId+"&pwd="+pwd+"&mobile="+mobile+"&content="+msgContent+"&encode="+encode;*/
		/*String strAddr = addr + 
				"?ac=send&uid="+userId+
				"&pwd="+pwd+
				"&mobile="+mobile+
				"&encode="+encode+
				"&content=" + msgContent;*/
		String strAddr =addr+"?ac=send&uid="+userId+"&pwd="+pwd+"&template="+100000+"&mobile="+mobile+"&content="+msgContent+"&encode="+encode;

		StringBuffer sb=new StringBuffer(strAddr);
		
		//发送请求
		URL url=new URL(sb.toString());
		HttpURLConnection connection=(HttpURLConnection)url.openConnection();
		connection.setRequestMethod("POST");
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(),"utf-8"));
		//返回结果
		String inputline=in.readLine();
		return inputline;
	}
	/**
	 * 生成6位数字
	 */
	
	static Random r=new Random();
	public static String getCode(){
		int n1=r.nextInt(900)+100;
		int n2=r.nextInt(900)+100;
		return n1+""+n2;
	}
}
